package com.ahsan.test.careaxiomtestapp;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;

import com.ahsan.test.careaxiomtestapp.contractor.BaseNetworkContractor;
import com.ahsan.test.careaxiomtestapp.utils.AppDataBase;
import com.ahsan.test.careaxiomtestapp.utils.NotificationUtil;


public abstract class BaseActivity extends AppCompatActivity implements BaseNetworkContractor {
    @Override
    public void onError(String errorMsg) {
        NotificationUtil.getInstance().showToast(this, errorMsg);
    }

    @Override
    public void onShowLoading() {
        NotificationUtil.getInstance().showLoading(this);
    }

    @Override
    public void onHideLoading() {
        NotificationUtil.getInstance().dismissLoading();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AppDataBase.destroyInstance();
    }

    /**
     * @return
     */
    public boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert cm != null;
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }
}
