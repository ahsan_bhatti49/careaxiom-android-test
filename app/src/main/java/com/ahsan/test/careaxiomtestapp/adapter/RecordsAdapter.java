package com.ahsan.test.careaxiomtestapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ahsan.test.careaxiomtestapp.R;
import com.ahsan.test.careaxiomtestapp.model.dao.DataModel;
import com.ahsan.test.careaxiomtestapp.utils.ImageDownloadUtil;

import java.util.List;

public class RecordsAdapter extends RecyclerView.Adapter<RecordsAdapter.RecordViewHolder> {

    private List<DataModel> recordModels;
    private Context context;

    public RecordsAdapter(List<DataModel> recordModels, Context context) {
        this.recordModels = recordModels;
        this.context = context;
    }

    @NonNull
    @Override
    public RecordViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_album_record_layout, parent, false);

        return new RecordViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecordViewHolder recordViewHolder, int i) {
        try{
            recordViewHolder.name.setText(recordModels.get(i).getTitle());
            ImageDownloadUtil.setImageViewWithLoader(context, recordModels.get(i).getThumbnailUrl(), recordViewHolder.progressBar, recordViewHolder.imageView);

        }catch (Exception e){
            Log.d(RecordsAdapter.class.getName(),e.getLocalizedMessage());
        }

    }

    @Override
    public int getItemCount() {
        return recordModels.size();
    }

    class RecordViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        ImageView imageView;
        ProgressBar progressBar;


        RecordViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.tvId);
            imageView = view.findViewById(R.id.ivThumbnail);
            progressBar = view.findViewById(R.id.pbImageLoader);

        }
    }

}
