package com.ahsan.test.careaxiomtestapp.model.retrofit;


import com.google.gson.JsonArray;
import com.google.gson.JsonObject;


import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * Created by ahsan on 11/09/2018.
 */

public interface NetworkCalls {

    @GET("/photos")
    Observable<JsonArray> getResponse();
}
