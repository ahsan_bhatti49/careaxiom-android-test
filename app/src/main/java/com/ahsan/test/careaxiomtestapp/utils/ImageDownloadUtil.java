package com.ahsan.test.careaxiomtestapp.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.ahsan.test.careaxiomtestapp.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

/**
 * Created by ahsan on 14/01/2019.
 */

public class ImageDownloadUtil {

    public static void setImageViewWithLoader(Context context, String url, final ProgressBar loader, ImageView imageView) {
        Glide.with(context).load(url)
                .apply(new RequestOptions().error(R.drawable.ic_error_black_24dp))
                .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).fitCenter()
                        .dontAnimate())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e,
                                                Object model, Target<Drawable> target, boolean isFirstResource) {
                        if (loader != null) {
                            loader.setVisibility(View.GONE);
                        }
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource,
                                                   Object model, Target<Drawable> target, DataSource dataSource,
                                                   boolean isFirstResource) {
                        if (loader != null) {
                            loader.setVisibility(View.GONE);
                        }
                        return false;
                    }
                }).into(imageView);

    }
}
