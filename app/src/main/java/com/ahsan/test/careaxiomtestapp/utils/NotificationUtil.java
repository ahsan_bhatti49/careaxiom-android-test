package com.ahsan.test.careaxiomtestapp.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ahsan.test.careaxiomtestapp.R;

import java.util.Objects;

/**
 * Created by ahsan on 28/09/2018.
 */

public class NotificationUtil {
    private static NotificationUtil notificationUtils;
    private Dialog dialog = null;

    /**
     * @return
     */
    public static NotificationUtil getInstance() {
        if (notificationUtils == null)
            notificationUtils = new NotificationUtil();

        return notificationUtils;
    }

    /**
     * @param context
     */
    public void showLoading(Context context) {
        dialog = new Dialog(context);
        if (!dialog.isShowing()) {
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.loader_dialog);
            Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCancelable(false);
            dialog.show();
        }
    }


    /**
     * @param context
     * @param msg
     */
    public void showToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    public void dismissLoading() {
        if (dialog != null) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }
    }

}
