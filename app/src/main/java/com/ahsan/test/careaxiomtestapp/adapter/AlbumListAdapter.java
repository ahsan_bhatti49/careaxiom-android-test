package com.ahsan.test.careaxiomtestapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ahsan.test.careaxiomtestapp.MainActivity;
import com.ahsan.test.careaxiomtestapp.R;
import com.ahsan.test.careaxiomtestapp.model.dao.DataModel;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;

public class AlbumListAdapter extends RecyclerView.Adapter<AlbumListAdapter.AlbumViewHolder> {

    private LinkedHashMap<String, List<DataModel>> dataModels;
    private Context context;

    public AlbumListAdapter(LinkedHashMap<String, List<DataModel>> albums, MainActivity mainActivity) {
        this.dataModels = albums;
        this.context = mainActivity;
    }

    @NonNull
    @Override
    public AlbumViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_album_layout, parent, false);

        return new AlbumViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull AlbumViewHolder albumViewHolder, int i) {
        try {
            albumViewHolder.name.setText("Album Id : " + Objects.requireNonNull(dataModels.keySet().toArray())[i].toString());
            albumViewHolder.recordsAdapter(Objects.requireNonNull(dataModels.keySet().toArray())[i].toString());

        } catch (Exception e) {
            Log.d(AlbumListAdapter.class.getName(), e.getLocalizedMessage());
        }
    }

    @Override
    public int getItemCount() {
        return this.dataModels.size();
    }

    class AlbumViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        RecyclerView rvAlbumRecords;

        AlbumViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.tvAlbumId);
            rvAlbumRecords = view.findViewById(R.id.rvAlbumRecords);
            LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            rvAlbumRecords.setLayoutManager(layoutManager);

        }

        private void recordsAdapter(String albumId) {
            RecordsAdapter adapter = new RecordsAdapter(dataModels.get(albumId), context);
            rvAlbumRecords.setAdapter(adapter);
        }
    }


}
