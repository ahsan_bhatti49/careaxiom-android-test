package com.ahsan.test.careaxiomtestapp.presenter.implementators;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.ahsan.test.careaxiomtestapp.MainActivity;
import com.ahsan.test.careaxiomtestapp.contractor.BaseNetworkContractor;
import com.ahsan.test.careaxiomtestapp.model.NetworkCommunicator.NetworkCommunicationInteractor;
import com.ahsan.test.careaxiomtestapp.model.NetworkCommunicator.NetworkResponseListener;
import com.ahsan.test.careaxiomtestapp.model.dao.DataModel;
import com.ahsan.test.careaxiomtestapp.presenter.callbacks.BaseNetworkResponseListener;
import com.ahsan.test.careaxiomtestapp.utils.AppDataBase;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import io.reactivex.Flowable;


/**
 * Created by ahsan on 16/10/2018.
 */

public class BaseImplementator implements BaseNetworkResponseListener, NetworkResponseListener.onResponseListener {
    private BaseNetworkContractor baseNetworkContractor;
    private NetworkResponseListener networkResponseListener;
    private boolean showLoading = false;

    public BaseImplementator(BaseNetworkContractor ckUserContractor) {
        this.baseNetworkContractor = ckUserContractor;
        networkResponseListener = new NetworkCommunicationInteractor();
    }

    @Override
    public void onError() {
        if (showLoading)
            baseNetworkContractor.onHideLoading();
        baseNetworkContractor.onError("Server is not responding");
    }

    @Override
    public void onSuccess(List<DataModel> response) {

        if (showLoading)
            baseNetworkContractor.onHideLoading();
        baseNetworkContractor.getApiResult(response);
    }

    @Override
    public void onFailure(String message) {
        if (showLoading)
            baseNetworkContractor.onHideLoading();
        baseNetworkContractor.onError(message);
    }

    @Override
    public void getApiResponse(Context context, boolean showLoader) {
        if (isNetworkAvailable(context)) {
            if (showLoader) {
                showLoading = true;
                baseNetworkContractor.onShowLoading();
                networkResponseListener.getResponse(context, this);
            } else {
                showLoading = false;
                networkResponseListener.getResponse(context, this);
            }

        } else {
            baseNetworkContractor.onError("No internet connection available");
        }
    }

    private boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert cm != null;
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    /**
     * @param response
     * @return method to convert data model to linked hashmap
     * so that it would be easier to populate data in recyclerviews
     */
    public LinkedHashMap<String, List<DataModel>> convertLisToHashMap(List<DataModel> response) {
        int prevAlbumId = 1, currentAlbumId = -1;
        List<DataModel> list = new ArrayList<>();
        LinkedHashMap<String, List<DataModel>> linkedHashMap = new LinkedHashMap<>();
        for (int i = 0; i < response.size(); i++) {

            currentAlbumId = response.get(i).getAlbumId();

            if (prevAlbumId != response.get(i).getAlbumId()) {

                linkedHashMap.put(String.valueOf(prevAlbumId), list);

                list = new ArrayList<>();

            }
            list.add(response.get(i));

            prevAlbumId = currentAlbumId;
        }
        return linkedHashMap;
    }

    /**
     * @param response
     * @param mainActivity
     */
    public void saveDataToLocalDb(List<DataModel> response, MainActivity mainActivity) {
        for (int i = 0; i < response.size(); i++) {
            try {
                AppDataBase.getAppDatabase(mainActivity).userDao().insertAll(response.get(i));
            } catch (Exception e) {
                Log.d(MainActivity.class.getName(), e.getLocalizedMessage());
            }
        }

    }


    /**
     * @param mainActivity
     * @return
     */
    public Flowable<List<DataModel>> getListFromLocalStorageRxjava(MainActivity mainActivity) {
        return AppDataBase.getAppDatabase(mainActivity).userDao().getAllRxjava();
    }
}
