package com.ahsan.test.careaxiomtestapp.contractor;


import com.ahsan.test.careaxiomtestapp.model.dao.DataModel;

import java.util.List;

/**
 * Created by ahsan on 16/10/2018.
 */

public interface BaseNetworkContractor {

    void onError(String errorMsg);

    void onShowLoading();

    void onHideLoading();

    void getApiResult(List<DataModel> response);

}

