package com.ahsan.test.careaxiomtestapp.presenter.callbacks;

public interface OnLoadMoreListener {
    void onLoadMore();
}