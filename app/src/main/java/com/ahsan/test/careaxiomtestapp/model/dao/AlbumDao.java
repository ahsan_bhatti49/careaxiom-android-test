package com.ahsan.test.careaxiomtestapp.model.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public interface AlbumDao {

    @Query("SELECT * FROM data_model")
    List<DataModel> getAll();

    @Query("SELECT * FROM data_model")
    Flowable<List<DataModel>> getAllRxjava();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(DataModel... albums);

    @Query("DELETE FROM data_model")
    void deleteTable();

}

