package com.ahsan.test.careaxiomtestapp;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.ahsan.test.careaxiomtestapp.adapter.AlbumListAdapter;
import com.ahsan.test.careaxiomtestapp.contractor.BaseNetworkContractor;
import com.ahsan.test.careaxiomtestapp.model.dao.DataModel;
import com.ahsan.test.careaxiomtestapp.presenter.implementators.BaseImplementator;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends BaseActivity implements BaseNetworkContractor {

    private BaseImplementator baseImplementator;
    private RecyclerView rvAlbumList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initUI();
        initMVP();
        getAllData();

    }

    private void initUI() {
        rvAlbumList = findViewById(R.id.rvAlbumList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvAlbumList.setLayoutManager(layoutManager);

    }

    @SuppressLint("CheckResult")
    private void getAllData() {
        onShowLoading();
        if (isNetworkAvailable()) {
            baseImplementator.getApiResponse(this, false);
        } else {
            onError("Fetching Data from Local Storage");
            baseImplementator.getListFromLocalStorageRxjava(this)
                    .subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::setAdapter, e -> onError("Error occurred!"));

        }
    }

    private void initMVP() {
        baseImplementator = new BaseImplementator(this);
    }

    @Override
    public void getApiResult(List<DataModel> response) {
        baseImplementator.saveDataToLocalDb(response, this);
        setAdapter(response);
    }

    /**
     * @param response
     */
    private void setAdapter(List<DataModel> response) {
        if (response != null && !response.isEmpty()) {
            AlbumListAdapter adapter = new AlbumListAdapter(baseImplementator.convertLisToHashMap(response), this);

            rvAlbumList.setAdapter(adapter);
        }
        onHideLoading();
    }

}
