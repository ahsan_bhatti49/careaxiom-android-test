package com.ahsan.test.careaxiomtestapp.presenter.callbacks;

import android.content.Context;

/**
 * Created by ahsan on 16/10/2018.
 */

public interface BaseNetworkResponseListener {
    void getApiResponse(Context context, boolean showLoader);
}
