package com.ahsan.test.careaxiomtestapp.model.NetworkCommunicator;

import android.content.Context;
import android.support.annotation.NonNull;


import com.ahsan.test.careaxiomtestapp.model.dao.DataModel;
import com.ahsan.test.careaxiomtestapp.model.retrofit.NetworkCalls;
import com.ahsan.test.careaxiomtestapp.model.retrofit.RetrofitClient;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by ahsan on 16/10/2018.
 */

public class NetworkCommunicationInteractor implements NetworkResponseListener {

    private JsonArray finalResponse;
    private onResponseListener responseListener;


    /**
     * @return
     */
    private io.reactivex.Observable<JsonArray> getObservable() {

        return RetrofitClient.getRetrofitClient()
                .create(NetworkCalls.class)
                .getResponse()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()
                );
    }


    /**
     * @return
     */
    private DisposableObserver<JsonArray> getObserver() {

        return new DisposableObserver<JsonArray>() {
            @Override
            public void onNext(@NonNull JsonArray response) {
                finalResponse = response;
            }

            @Override
            public void onError(@NonNull Throwable e) {
                responseListener.onFailure("Network connection failed. Please verify your connection and try again.");
            }

            @Override
            public void onComplete() {
                try {
                    if ((!finalResponse.equals("")) && responseListener != null) {

                        responseListener.onSuccess(getList(finalResponse));
                    }
                } catch (Exception e) {
                    responseListener.onFailure("Something went wrong please try again");
                }
            }
        };
    }

    /**
     * @param finalResponse
     * @return
     */
    private List<DataModel> getList(JsonArray finalResponse) {
        List<DataModel> responeList = new Gson().fromJson(finalResponse.toString(), new TypeToken<List<DataModel>>() {
        }.getType());

        return responeList;
    }

    /**
     * @param context
     * @param onResponseListener
     */

    @Override
    public void getResponse(Context context, onResponseListener onResponseListener) {
        if (onResponseListener != null) {
            responseListener = onResponseListener;
            getObservable().subscribeWith(getObserver());

        }
    }
}

