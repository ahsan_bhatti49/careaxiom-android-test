package com.ahsan.test.careaxiomtestapp.model.NetworkCommunicator;

import android.content.Context;

import com.ahsan.test.careaxiomtestapp.model.dao.DataModel;

import java.util.List;


/**
 * Created by ahsan on 11/09/2018.
 */

public interface NetworkResponseListener {
    interface onResponseListener {
        void onError();

        void onSuccess(List<DataModel> response);

        void onFailure(String message);

    }

    void getResponse(Context context, onResponseListener onResponseListener);
}
